augroup nroffHotkeys
  autocmd!
  autocmd FileType nroff inoremap <leader>t .TL<cr>
  autocmd FileType nroff inoremap <leader>a .AU<cr>
  autocmd FileType nroff inoremap <leader>nh .NH 1<cr>
  autocmd FileType nroff inoremap <leader>p .PP<cr>
  autocmd FileType nroff inoremap <S-CR> <cr>.br<cr>
  autocmd FileType nroff inoremap <leader>8 .IP \(bu 3<cr>
augroup END

nnoremap <leader>cl yyPcc.ce 1<Esc>jpcc.ce 0<Esc>jo<cr>

function! MakeSelectionItalic()
  normal! di<cr>.I "<Esc>pa"<cr>
endfunction
function! CenterLine()
  normal! di<cr>.I "<Esc>pa"<cr>
endfunction
function! MakeSelectionBold()
 normal! di<cr>.B "<Esc>pa"<cr>
endfunction

vnoremap <leader>b :call MakeSelectionBold()<cr>
vnoremap <leader>i :call MakeSelectionItalic()<cr>
