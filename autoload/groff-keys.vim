augroup GroffHotkeys
  autocmd!
  autocmd FileType groff inoremap <leader>t .TL<cr>
  autocmd FileType groff inoremap <leader>a .AU<cr>
  autocmd FileType groff inoremap <leader>nh .NH 1<cr>
  autocmd FileType groff inoremap <leader>p .PP<cr>
  autocmd FileType groff inoremap <S-CR> <cr>.br<cr>
  autocmd FileType groff inoremap <leader>8 .IP \(bu 3<cr>
augroup END

function! MakeSelectionItalic()
  normal! di<cr>.I "<Esc>pa"<cr>
endfunction
function! MakeSelectionBold()
 normal! di<cr>.B "<Esc>pa"<cr>
endfunction

vnoremap <leader>b :call MakeSelectionBold()<cr>
vnoremap <leader>i :call MakeSelectionItalic()<cr>
